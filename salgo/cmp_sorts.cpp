/* */
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdio>

#include <cstdlib>

#include <sys/time.h>
#include <ctime>


using namespace std;

const char* OUTPUT_FILENAME = "output.txt";
const char MARKER = 'a' -1;
const int MAXLENGTH = 17;

/* Utility Function Prototypes */
bool isCorrectlySorted(char **data, int size);
char **Duplicate(char **original, int size);
void FreeMemory(char **data, int size);
char *Duplicate(char *original);
void Swap(char **a, char **b);


/* Sort Function Prototypes*/
void BubbleSort(char **data, int size);

void QuickSort(char **data, int size);
void QuickSort(char **data, int first, int last);
int Partition(char **data, int p, int q);


void HeapSort(char **data, const int size);
void MaxHeapify(char **data, const int parentIndex, const int heapSize);
void BuildMinHeap(char **data, const int heapSize);

int Parent(int index);
int Left(int index);
int Right(int index);


void RadixSort(char **data, int size);
char **StableSort(char **data, int baseIndex, int size);
char **DuplicateForRadixSort(char **original, int size);


/* Hybrid Randomized quick sort and insertion sort
 *
 * The running time of quicksort is improved by taking advantage of the fast running time of insertion sort
 * when the input is almost sorted.
 */
void HybridSort(char **data, int size);


int Sort(void (*sort)(char **, int), char **data, int size){
	struct timeval tv_begin;
	struct timeval tv_end;

	int passed_milliseconds;

	gettimeofday(&tv_begin, NULL);
	(*sort)(data, size);
	gettimeofday(&tv_end, NULL);

	passed_milliseconds = (tv_end.tv_sec - tv_begin.tv_sec) * 1000 + (tv_end.tv_usec - tv_begin.tv_usec)/1000;
	return passed_milliseconds;
}


int main(int argc, char *argv[]){
	if (argc < 2){
	    cout << "usage: compare input_file_name" << endl;
	    return 1;
	}
	
	FILE * pFile = fopen(argv[1], "r");
	ofstream cout(OUTPUT_FILENAME);

	if (pFile == NULL){
		perror("Error opening input file!");
		return 1;
	}


	int n, k;
	char dummy;
	fscanf(pFile, "%d%c%d", &n, &dummy, &k);

	int *sortHowMany = new int[k];
	int *whichSortAlgo = new int[k];

	for (int i = 0; i < k; i++){
		fscanf(pFile, "%d%c", &sortHowMany[i], &dummy);
		fscanf(pFile, "%d", &whichSortAlgo[i]);
	}

	fscanf(pFile, "%c", &dummy);


	char **inputData = new char*[n];

	for (int i = 0; i < n; i++){
		inputData[i] = new char[MAXLENGTH];
	}

	for (int i = 0; i < n; i++){
		fgets(inputData[i], MAXLENGTH, pFile);			// fgets function reads the newline character!

		char *newline = strchr(inputData[i], '\n');		// search for the newline character
		if (newline != NULL){
			*newline = '\0';				// if found, replace the newline character with the null terminator
		}
	}

	fclose(pFile);										

	char **data = NULL;

	for (int i = 0; i < k; i++){
		cout << sortHowMany[i];
		switch(whichSortAlgo[i]){
		case 1:
			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(HybridSort, data, sortHowMany[i]);
			if (i != k-1)
				FreeMemory(data, sortHowMany[i]);
			cout << "0 0 0 0";
			break;

		case 2:
			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(HybridSort, data, sortHowMany[i]);

			FreeMemory(data, sortHowMany[i]);
			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(HeapSort, data, sortHowMany[i]);

			if (i != k-1)
				FreeMemory(data, sortHowMany[i]);
			cout << " 0 0 0";
			break;

		case 3:
			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(HybridSort, data, sortHowMany[i]);
			FreeMemory(data, sortHowMany[i]);

			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(HeapSort, data, sortHowMany[i]);
			FreeMemory(data, sortHowMany[i]);

			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(QuickSort, data, sortHowMany[i]);

			if (i != k-1)
				FreeMemory(data, sortHowMany[i]);
			cout << " 0 0";
			break;
		case 4:
			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(HybridSort, data, sortHowMany[i]);
			FreeMemory(data, sortHowMany[i]);

			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(HeapSort, data, sortHowMany[i]);
			FreeMemory(data, sortHowMany[i]);

			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(QuickSort, data, sortHowMany[i]);
			FreeMemory(data, sortHowMany[i]);

			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(BubbleSort, data, sortHowMany[i]);

			if (i != k-1)
				FreeMemory(data, sortHowMany[i]);
			cout << " 0";
			break;

		case 5:
			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(HybridSort, data, sortHowMany[i]);
			FreeMemory(data, sortHowMany[i]);

			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(HeapSort, data, sortHowMany[i]);
			FreeMemory(data, sortHowMany[i]);

			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(QuickSort, data, sortHowMany[i]);
			FreeMemory(data, sortHowMany[i]);

			data = Duplicate(inputData, sortHowMany[i]);
			cout << " " << Sort(BubbleSort, data, sortHowMany[i]);
			FreeMemory(data, sortHowMany[i]);

			data = DuplicateForRadixSort(inputData, sortHowMany[i]);
			cout << " " << Sort(RadixSort, data, sortHowMany[i]);
			if (i != k-1)
				FreeMemory(data, sortHowMany[i]);
			break;
		}
		cout << endl;
	}

	for (int i = 0; i < sortHowMany[k-1]; i++){
		cout << data[i] << endl;
	}

	FreeMemory(data, sortHowMany[k-1]);

	return 0;
}


char **Duplicate(char **original, int size){
	char** newArray = new char*[size];
	if (newArray == NULL){
		cout << "Insufficient Memory! Exiting...";
		exit(0);
	}

	for (int i = 0; i < size; i++){
		int len = strlen(original[i]);
		newArray[i] = new char[len+1];
		for (int j = 0; j < len; j++){
			newArray[i][j] = original[i][j];
		}
		newArray[i][len] = '\0';
	}
	return newArray;
}


char *Duplicate(char *original){
	int len = strlen(original);
	char *ret = new char[len+1];
	for (int i = 0; i < len; i++){
		ret[i] = original[i];
	}
	ret[len] = '\0';
	return ret;
}

void BubbleSort(char **data, int size){
	int n = size;
	int times = 0;
	for (int i = n; i > 0; i--){
		int newn = 0;
		for (int j = 1; j <= n-1; j++){
			times++;
			if (strcmp(data[j], data[j-1]) <= 0){
				Swap(&data[j], &data[j-1]);
				newn = j;
			}
		}
		n = newn;								// minor optimization
	}
}

void Swap(char **a, char **b){
	int l1 = strlen(*a);
	int l2 = strlen(*b);

	char *dupA = new char[l1];
	for (int i = 0; i < l1; i++){
		dupA[i] = (*a)[i];
	}

	delete *a;
	*a = new char[l2+1];
	for (int i = 0; i < l2; i++){
		(*a)[i] = (*b)[i];
	}
	(*a)[l2] = '\0';

	delete *b;
	*b = new char[l1+1];

	for (int i = 0; i < l1; i++){
		(*b)[i] = dupA[i];
	}
	(*b)[l1] = '\0';

	delete[] dupA;
}

void QuickSort(char **data, int first, int last){
	if (first < last){
		int q = Partition(data, first, last);
		QuickSort(data, first, q-1);
		QuickSort(data, q+1, last);
	}
}

void QuickSort(char **data, int size){
	QuickSort(data, 0, size-1);
}

int Partition(char **data, int p, int r){
	int i = p-1;
	char *x = Duplicate(data[r]);

	for (int j = p; j < r; j++){
		if (strcmp(data[j], x) <= 0){
			i++;
			Swap(&data[i], &data[j]);
		}
	}
	Swap(&data[i+1], &data[r]);
	delete [] x;
	return i+1;
}




int Parent(int index){
	return index%2 != 0? (index >> 1) : (index >> 1) - 1;
}

int Left(int index){
	return (index << 1) + 1;
}

int Right(int index){
	return (index << 1) + 2;
}


void MaxHeapify(char **data, const int parentIndex, const int heapSize){
	int leftIndex = Left(parentIndex);
	int rightIndex = Right(parentIndex);
	int largestIndex = parentIndex;

	if (leftIndex < heapSize && strcmp(data[leftIndex], data[parentIndex]) > 0){
		largestIndex = leftIndex;
	}
	if (rightIndex < heapSize && strcmp(data[rightIndex], data[largestIndex]) > 0){
		largestIndex = rightIndex;
	}
	if (largestIndex != parentIndex){
		Swap(&data[largestIndex], &data[parentIndex]);
		MaxHeapify(data, largestIndex, heapSize);
	}
}


void BuildMinHeap(char **data, const int heapSize){
	for (int k = heapSize/2; k >= 0; k--){
		MaxHeapify(data, k, heapSize);
	}
}

void HeapSort(char **data, const int size){
	int heapSize = size;
	BuildMinHeap(data, heapSize);
	for (int i = size-1; i >= 0; i--){
		Swap(&data[0], &data[i]);
		heapSize--;
		MaxHeapify(data, 0, heapSize);
	}
}

void RadixSort(char **data, int size){
	char **temp = Duplicate(data, size);
	for (int i = MAXLENGTH-1; i >= 0; i--){
		char **_temp = temp;
		temp = StableSort(temp, i, size);
		FreeMemory(_temp, size);
	}

	for (int i = 0; i < size; i++){
		int len = (int)temp[i][MAXLENGTH];
		temp[i][len] = '\0';
		for (int j = 0; j < len; j++){
			data[i][j] = temp[i][j];
		}
		data[i][len] = '\0';
	}
	FreeMemory(temp, size);
}


char ** StableSort(char **data, int baseIndex, int size){
	int *c = new int['z' + 1];

	/* c[',a,b....z] = 0 */
	for (int i = 'a'-1; i <= 'z'; i++){
		c[i] = 0;
	}

	for (int i = 0; i < size; i++){
		c[(int(data[i][baseIndex]))]++;
	}

	for (int i = 'a'; i <= 'z'; i++){
		c[i] = c[i] + c[i-1];
	}

	char **newdata = new char*[size+5];


	for (int j = size-1; j >= 0; j--){
		newdata[c[(int(data[j][baseIndex]))]-1] = Duplicate(data[j]);
		c[(int(data[j][baseIndex]))]--;
	}

	/* Memory cleanup */
	delete c;

	return newdata;
}

char **DuplicateForRadixSort(char **original, int size){
	char **newdata = new char*[size];
	for (int i = 0; i < size; i++){
		newdata[i] = new char[MAXLENGTH+5];
	}

	for (int i = 0; i < size; i++){
		int len = strlen(original[i]);
		for (int j = 0; j < MAXLENGTH; j++){
			if (j >= len){
				newdata[i][j] = 'a'-1;
			}
			else newdata[i][j] = original[i][j];
		}
		newdata[i][MAXLENGTH] = strlen(original[i]);
		newdata[i][MAXLENGTH+1] = '\0';
	}
	return newdata;
}

bool isCorrectlySorted(char **data, int size){
	for (int i = 1; i < size; i++){
		if (strcmp(data[i-1], data[i]) > 0)
			return false;
	}

	return true;
}


void FreeMemory(char **data, int size){
	for (int i = 0; i < size; i++){
		delete data[i];
	}
	delete[] data;
}

int Hybrid_Partition(char **data, int first, int last){
	srand((unsigned)time(0));

	int randomInteger = rand() %(last-first) + first;
	Swap(&data[randomInteger], &data[last]);
	return Partition(data, first, last);
}

void Hybrid_QuickSort(char **data, int first, int last, int k){
	if (first < last){
		int q = Hybrid_Partition(data, first, last);

		if (q - first > k)
			Hybrid_QuickSort(data, first, q-1, k);

		if (last - q > k)
			Hybrid_QuickSort(data, q+1, last, k);
	}
}


void HybridSort(char **data, int size){
	int k = size / 25;
	if (k > 350)
		k = 350;
	Hybrid_QuickSort(data, 0, size-1, k);

	for (int j = 1; j < size; j++){
		char *key = data[j];
		int i = j-1;
		while (i >= 0 && strcmp(data[i], key) > 0){
			data[i+1] = data[i];
			i--;
		}
		data[i+1] = key;
	}
}
